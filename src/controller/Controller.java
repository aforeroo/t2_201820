package controller;



import api.IDivvyTripsManager;
import model.data_structures.Lista;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller {

	
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadStations() {
		manager.loadStations("./docs/Divvy_Stations_2017_Q3Q4.csv");
		
	}
	
	public static void loadTrips() {
		manager.loadTrips("./docs/Divvy_Trips_2017_Q4.csv");
	}
		
	public static Lista <VOTrip> getTripsOfGender (String gender) {
		return manager.getTripsOfGender(gender);
	}
	
	public static Lista <VOTrip> getTripsToStation (int stationID) {
		return manager.getTripsToStation(stationID);
	}
	
	
	
}
