package view;

import java.io.File;

import java.util.Scanner;
import controller.Controller;
import model.data_structures.Lista;
import model.vo.VOTrip;

public class DivvyTripsManagerView 
{
	public static void main(String[] args) 
	{
		File file = new File(".");
		System.out.println(file.getAbsolutePath());
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					
					Controller.loadStations();							
					break;
					
				case 2:
						
					Controller.loadTrips();					
					break;
					
				case 3:
					System.out.println("Ingrese el genero:");
					String gender = sc.next();
					
					long inicio1 = System.currentTimeMillis(); 
					Lista<VOTrip> bykeTripsList = Controller.getTripsOfGender (gender);
					long fin1 = System.currentTimeMillis();	
					
					
					long inimostrar1 = System.currentTimeMillis();
					for (VOTrip trips : bykeTripsList) 
					{
						System.out.println(trips.getTripId()+ " " + trips.getTripDuration() + " " + trips.getFromstationName() + " " + trips.getToStationName());
					}
					long finmostrar1 = System.currentTimeMillis();
					
					System.out.println("\n\n  " + bykeTripsList.size() + " Resultados encontrados   en  " + ((double)(fin1 - inicio1)/(1000)) + " segundos  |  mostrados en " + ((double)(finmostrar1- inimostrar1)/(1000)) + " milisegundos \n\n");
					
					break;
					
				case 4:
					System.out.println("Ingrese el identificador de la estaci�n:");
					int stationId = Integer.parseInt(sc.next());
					
					long inicio2 = System.currentTimeMillis(); 
					Lista <VOTrip> bykeTripsList2 = Controller.getTripsToStation(stationId);
					long fin2 = System.currentTimeMillis();		
					
													
					long inimostrar2 = System.currentTimeMillis();
					for (VOTrip trips : bykeTripsList2) 
					{
						System.out.println("To sation # " + stationId+ ": " + trips.getTripId() + " " + trips.getTripDuration() + " " + trips.getFromstationName() + " " + trips.getToStationName());
					}
					long finmostrar2 = System.currentTimeMillis();
					
					System.out.println("\n\n  " + bykeTripsList2.size() + " Resultados encontrados   en  " + ((double)(fin2 - inicio2)/(1000)) + " segundos  |  mostrados en " + ((double)(finmostrar2- inimostrar2)/(1000)) + " segundos \n\n");
					break;
					
				case 5:	
					sc.close();
					System.exit(1);
					break;
			}
		}
	}
	
	

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 2----------------------");
		System.out.println("----------ANDRES FORERO OSORIO: 201614341-----------\n\n");
		System.out.println("fuente de datos: sistema publico de bicicletas Chicago | portal: www.divvybykes.com/system-data?\n\n   ");
		System.out.println("1. Cree una nueva coleccion de estaciones");
		System.out.println("2. Cree una nueva coleccion de viajes");
		System.out.println("3. Dar listado de viajes realizados dado un genero");
		System.out.println("4. Dar listado de viajes que finalizan en una estaci�n de bicicletas espec�fica");
		System.out.println("5. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
	
	
	
}
