package model.logic;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.Lista;
import model.data_structures.ListaDobleEncadenada;


public class DivvyTripsManager implements IDivvyTripsManager {

	
	
	
	public final static String RUTA_ESTACIONES = "./docs/Divvy_Stations_2017_Q3Q4.csv";
	
	public final static String RUTA_TRIPS = "./docs/Divvy_Trips_2017_Q4.csv";
	
	public Lista<VOStation> estaciones = new ListaDobleEncadenada<>();
	
	public Lista<VOTrip> trips = new ListaDobleEncadenada<>();
	
	
	
	public void loadStations (String stationsFile) {
		int contador = 0;
		BufferedReader br = null;
		try {
			
			br = new BufferedReader(new FileReader(RUTA_ESTACIONES));
			String line = br.readLine();
			line = br.readLine();
			long inicio = System.currentTimeMillis();
			while(line != null)
			{
				String[] campos = line.split(",");
				
				estaciones.addEnd(parserStation(campos));
				contador++;
				
				line = br.readLine();				
			}
			long fin = System.currentTimeMillis();
			System.out.println("\n\n CARGA EXITOSA !!" );
			System.out.println("\n\n  " + contador + "	registros cargados en: " + ((double)(fin-inicio)/(1000)) + " segundos\n\n" );
			
					
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("error tratando de cargar los datos");
		}finally
		{
			if(br != null)
			{
				try {
					br.close();
				} catch (IOException e) {
					
					e.printStackTrace();
				}
			}
		}			
		
	}

	
	
	public void loadTrips (String tripsFile) {
		int contador = 0;
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(RUTA_TRIPS));
			String line = br.readLine();// linea de formato que no cuenta
			line = br.readLine();
			
			long inicio = System.currentTimeMillis();
			while(line != null)
			{
				String[] campos = line.split(",");
				
				trips.addEnd(parserTrips(campos));
				contador++;
				line = br.readLine();
			}
			
			long fin = System.currentTimeMillis();
			System.out.println("\n\n CARGA EXITOSA !!" );
			System.out.println("\n\n  " + contador + "	registros cargados en: " + ((double)(fin-inicio)/(1000)) + " segundos\n\n" );
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("error tratando de cargar los datos");
		}finally
		{
			if(br != null)
			{
				try {
					br.close();
				} catch (IOException e) {
					
					e.printStackTrace();
				}
			}
		}
		
	}
	
	
	
	public static Date convertDate(String str)
	{
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date date = null;
		try {
			date = formatter.parse(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}
	
	
	
	public static VOStation parserStation(String[] campos)
	{
		VOStation actualStation;	
		
		actualStation = new VOStation(Integer.valueOf(campos[0]), campos[1], Double.valueOf(campos[3]),
									  Double.valueOf(campos[4]), Integer.valueOf(campos[5]),
									  convertDate(campos[6]));
		
		
		System.out.println( actualStation.getId() + " || " + actualStation.getName());
		return actualStation;
	}
	
		
	
	public static VOTrip parserTrips(String[] campos)
	{
		
		VOTrip actualTrip;
		
		String userType = campos[9];
		String gender = "";
		String birthYear = "";
		if(userType.equals("Subscriber"))
		{
			try {
				if(!campos[10].equals("") || !campos[10].equals(null))
					gender = campos[10];
				if(!campos[11].equals("") || !campos[10].equals(null))
					birthYear = campos[11];
			} catch (Exception e) {
				
			}
			
		}
		
		actualTrip = new VOTrip(Integer.valueOf(campos[0]), convertDate(campos[1]), convertDate(campos[2]),
								Integer.valueOf(campos[3]), Integer.valueOf(campos[4]), Integer.valueOf(campos[5]), campos[6],
								Integer.valueOf(campos[7]), campos[8], userType, gender, birthYear);
		System.out.println(actualTrip.getTripId());
		return actualTrip;
	}
		
	
		
	public Lista<VOTrip> getTripsOfGender (String gender) {
		Lista<VOTrip> listaTripGender = new ListaDobleEncadenada<>();
		Iterator<VOTrip> iterador = trips.iterator();
		VOTrip actual = iterador.next();
			
		while(iterador.hasNext())
		{
			if(actual.getGender().equalsIgnoreCase(gender))
			{
				listaTripGender.addEnd(actual);
			}
			actual = iterador.next();
			
		}
		
		return listaTripGender;
	}

	
	
	public Lista<VOTrip> getTripsToStation (int stationID) {

		Lista<VOTrip> listaTripToStation = new ListaDobleEncadenada<>();
		Iterator<VOTrip> iterador = trips.iterator();
		VOTrip actual = iterador.next();
		
				
		while(iterador.hasNext())
		{
			if(actual.getToStationId() == stationID)
			{
				listaTripToStation.addEnd(actual);
			}
			actual = iterador.next();
			
		}
		
		
		
		return listaTripToStation;
	}
	
		


}
