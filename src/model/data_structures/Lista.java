package model.data_structures;

import java.util.Iterator;



public interface Lista<E> extends Iterable<E>{
	
	public boolean isEmpty();	

	public void addFirst(E nuevo);
			
	public void addEnd(E item);	
	
	public int size();	
	
	public E getItemK(int k);
	
	public E removeK(int k);
	
	public Iterator<E> iterator();
}
