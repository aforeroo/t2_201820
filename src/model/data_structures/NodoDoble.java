package model.data_structures;

public class NodoDoble<E> {
	
	NodoDoble<E> prev;
	
	E item;
	
	NodoDoble<E> next;
		
	
	public NodoDoble(E objeto)
	{
		this(objeto, null, null);
	}
	
	public NodoDoble(E objeto, NodoDoble<E> pPrev, NodoDoble<E> pNext)
	{
		prev = pPrev;
		item = objeto;
		next = pNext;
				
	}

	public NodoDoble<E> getPrev() {
		return prev;
	}

	public E getItem() {
		return item;
	}

	public NodoDoble<E> getNext() {
		return next;
	}

}
