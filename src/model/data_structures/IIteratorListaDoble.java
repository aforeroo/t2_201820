package model.data_structures;

import java.util.Iterator;

public interface IIteratorListaDoble<E> extends Iterator<E>{

	public int getCurrentIndex();
	
	public E getCurrent();
	
	public boolean hasPrev();
	
	public E prev();
	
	public E removeCurrent();
	
	
}
