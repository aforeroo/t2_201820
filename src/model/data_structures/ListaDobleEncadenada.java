package model.data_structures;


import java.util.NoSuchElementException;

public class ListaDobleEncadenada<E> implements Lista<E> {

	NodoDoble<E> primero;
	
	NodoDoble<E> ultimo;
	
	int size;
	
	
	public ListaDobleEncadenada()
	{
		primero = ultimo = null;
		size = 0;
	}
	
	
	public boolean isEmpty() {
		
		return primero==null;
	}

	
	public void addFirst(E nuevo) {
		
		if(isEmpty())
			primero = ultimo = new NodoDoble<E>(nuevo);
		else
			primero = new NodoDoble<E>(nuevo, null, primero);
		
		size++;
	}

	
	public void addEnd(E item) {
		
		if(isEmpty())
			primero = ultimo = new NodoDoble<E>(item);
		else{
			NodoDoble<E> antiguoUltimo = ultimo;
			ultimo = ultimo.next = new NodoDoble<E>(item, antiguoUltimo , null);
		}
		size++;
	}

	
	public int size() {
		
		return size;
	}
	
	
	
	
	
	public E getItemK(int k)
	{
		IteradorListaDoble<E> iterador = iterator();
		E item = iterador.next();
		int contador = 1;
		while(contador != k && contador <= size)
		{
			item = iterador.next();		
			contador++;
		}
		
		return item;
		
	}
		
	
	public E removeK(int k){
		
		IteradorListaDoble<E> iterador = iterator();
		E item = iterador.next();
		int contador = 1;
		while(contador != k && contador <= size)
		{
			item = iterador.next();
			contador++;
		}
		item = iterador.removeCurrent();
		return item;
	}
	
	 

	
	public void mostrarEstructura() {
		
		int contador = 0;
		IteradorListaDoble<E> iterador = iterator();
		E item = iterador.next();
		
		while(iterador.hasNext())
		{
			System.out.println(item.toString());
			contador++;
			item = iterador.next();
		}
		
		System.out.println("\n\n # elementos estructura: " + contador + "\n\n");
		
	}

	
	public IteradorListaDoble<E> iterator() {		
		return new IteradorListaDoble<>(primero);
	}
	
	
	private class IteradorListaDoble<T> implements IIteratorListaDoble<T>
	{

		private NodoDoble<T> current;
		private int index;
		
		
		//constructor Iterador
		public IteradorListaDoble(NodoDoble<T> first)
		{
			current = first;
			index = 1;
		}
		
		
		public int getCurrentIndex()
		{
			return index;
		}
		
		public T getCurrent()
		{
			return current.item;
		}
		
		
		public boolean hasNext() {
			
			return current.next != null;
		}
		
		public boolean hasPrev()
		{
			return current.prev != null;
		}

		
		public T next() {
			
			if(!hasNext()) throw new NoSuchElementException();
			
			T item = current.item;
			current = current.next;
			index++;
			return item;
		}
		
		public T prev(){
			
			if(!hasPrev()) throw new NoSuchElementException();
			
			T item = current.prev.item;
			current= current.prev;
			index--;
			return item;
		}
		
		public T removeCurrent()
		{
			NodoDoble<T> oldCurrent = current;
			T item = oldCurrent.item;
			
			
			current = oldCurrent.prev;
			current.next = oldCurrent.next;
			oldCurrent = null;
			index--;
			return item;
			
		}	
		
	}

	

}
