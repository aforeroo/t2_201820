package model.vo;

import java.util.Date;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	

	private int tripId;
	
	private Date startTime;
	
	private Date endTime;
	
	private int bikeId;
	
	private int tripDuration; //Seconds
	
	private int fromStationId;
	
	private String fromstationName;
	
	private int toStationId;
	
	private String toStationName;
	
	private String userType;
	
	private String gender;
	
	private String birthyear;
	
	
	
	public VOTrip(int tripId, Date startTime, Date endTime, int bikeId,
			int tripDuration, int fromStationId, String fromstationName,
			int toStationId, String toStationName, String userType, String gender,
			String birthyear) {
		super();
		this.tripId = tripId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.bikeId = bikeId;
		this.tripDuration = tripDuration;
		this.fromStationId = fromStationId;
		this.fromstationName = fromstationName;
		this.toStationId = toStationId;
		this.toStationName = toStationName;
		this.userType = userType;
		this.gender = gender;
		this.birthyear = birthyear;
		
	}

	public int getTripId() {
		return tripId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public int getBikeId() {
		return bikeId;
	}

	public int getTripDuration() {
		return tripDuration;
	}

	public int getFromStationId() {
		return fromStationId;
	}

	public String getFromstationName() {
		return fromstationName;
	}

	public int getToStationId() {
		return toStationId;
	}

	public String getToStationName() {
		return toStationName;
	}

	public String getUserType() {
		return userType;
	}

	public String getGender() {
		return gender;
	}

	public String getBirthyear() {
		return birthyear;
	}
	
	public String toString()
	{
		return "tripId:" + tripId + "  |  duration(s):" + tripDuration + "  |  user:" + userType ;
	}

	
	
	
	
	

	
	
	
	
	
	
}
